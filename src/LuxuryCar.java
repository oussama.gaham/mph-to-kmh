public class LuxuryCar {
	private double speed;

	public LuxuryCar(double speed) {
		this.speed = speed;
	}

	public double getSpeed(){
		return speed;
	}
}
