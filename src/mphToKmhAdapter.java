public class mphToKmhAdapter implements SpeedAdapter {
	private LuxuryCar car;

	public mphToKmhAdapter(LuxuryCar c) {
		this.car = c;
	}

	@Override
	public double getSpeed() {
		//1mph = 1.609344kmh
		return car.getSpeed() * 1.609344;
	}
}
