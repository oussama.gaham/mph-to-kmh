public class Main {
	public static void main(String[] args) {

		LuxuryCar c = new LuxuryCar(260.0);
		System.out.printf("%fMPH\n", c.getSpeed());

		SpeedAdapter sa = new mphToKmhAdapter(c);
		System.out.printf("%fKMH\n", sa.getSpeed());

	}
}